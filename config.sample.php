<?php 

namespace javik\cvsupplier;

final class config {

    // SMTP Configuration
    private const SMTP_HOST = '';
    private const SMTP_PORT = '';
    private const SMTP_USER = '';
    private CONST SMTP_PASS = '';

    // GPG Key
    private const GPG_KEY        = '';
    private const GPG_PASSPHRASE = '';

    ///                           ///
    /// --- STOP EDITING HERE --- ///
    ///                           ///

    // Version
    private const VERSION_MAJOR = 1;
    private const VERSION_MINOR = 0;
    private const VERSION_PATCH = 0;

    // Getter
    // Why? Do have a cleaner syntax. So we have e.g. echo config::getVersion() . PHP_EOL; 
    // instead of echo config::VERSION_MAJOR . "." . config::VERSION_MINOR . "." . config::VERSION_PATCH; . PHP_EOL; 

    /**
     * Get SMTP Hostname from config
     * @return string
     */
    public static function getSMTPHost() :string {
        return self::SMTP_HOST;
    }

    /**
     * Get SMTP Port from config
     * @return int
     */
    public static function getSMTPPort() :int {
        return self::SMTP_PORT;
    }

    /**
     * Get SMTP user from config
     * @return string
     */
    public static function getSMTPUser() :string {
        return self::SMTP_USER;
    }

    /**
     * Get SMTP user password from config
     * @return string
     */
    public static function getSMTPPassword() :string {
        return self::SMTP_PASS;
    }

    /**
     * Get Path to GPG private Key from config
     * @return string
     */
    public static function getGPGKey() :string {
        return self::GPG_KEY;
    }

    /**
     * Get Passphrase for gpg private key
     * @return string
     */
    public static function getGPGPassphrase() :string {
        return self::GPG_PASSPHRASE;
    }

    /**
     * Get Version
     * @return string
     */
    public static function getVersion() :string {
        return self::VERSION_MAJOR . "." . self::VERSION_MINOR . "." . self::VERSION_PATCH;
    }

    /**
     * Get major version from config
     * @return string
     */
    public static function getVersionMajor() :string {
        return self::VERSION_MAJOR;
    }

    /**
     * Get minor version from config
     * @return string
     */
    public static function getVersionMinor() :string {
        return self::VERSION_MINOR;
    }

    /**
     * Get patch level from config
     * @return string
     */
    public static function getVersionPatch() :string {
        return self::VERSION_PATCH;
    }
}
